import math
from numbers import Number
from typing import Tuple, Union, List, Any, Optional

import numpy as np  # type: ignore

from satmath.base import BaseMatrix, EulerOrder, MathType, NpProxy


class Matrix44(BaseMatrix):

    _shape = (4, 4,)

    # m<c> style access
    m1 = NpProxy(0)
    m2 = NpProxy(1)
    m3 = NpProxy(2)
    m4 = NpProxy(3)

    # m<r><c> access
    m11 = NpProxy((0, 0))
    m12 = NpProxy((0, 1))
    m13 = NpProxy((0, 2))
    m14 = NpProxy((0, 3))
    m21 = NpProxy((1, 0))
    m22 = NpProxy((1, 1))
    m23 = NpProxy((1, 2))
    m24 = NpProxy((1, 3))
    m31 = NpProxy((2, 0))
    m32 = NpProxy((2, 1))
    m33 = NpProxy((2, 2))
    m34 = NpProxy((2, 3))
    m41 = NpProxy((3, 0))
    m42 = NpProxy((3, 1))
    m43 = NpProxy((3, 2))
    m44 = NpProxy((3, 3))

    # rows
    r1 = NpProxy(0)
    r2 = NpProxy(1)
    r3 = NpProxy(2)
    r4 = NpProxy(3)

    # columns
    c1 = NpProxy((slice(0, 4), 0))
    c2 = NpProxy((slice(0, 4), 1))
    c3 = NpProxy((slice(0, 4), 2))
    c4 = NpProxy((slice(0, 4), 3))

    @classmethod
    def identity(cls) -> 'Matrix44':
        return cls(np.identity(4))

    @classmethod
    def from_euler(cls, euler: 'Euler', order: Optional[EulerOrder] = None) -> 'Matrix44':
        assert isinstance(euler, Euler)

        order = order or euler.order

        a = math.cos(euler.x)
        b = math.sin(euler.x)
        c = math.cos(euler.y)
        d = math.sin(euler.y)
        e = math.cos(euler.z)
        f = math.sin(euler.z)

        data = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # np.zeros(16)

        if order == EulerOrder.XYZ:
            ae = a * e
            af = a * f
            be = b * e
            bf = b * f

            data[0] = c * e
            data[1] = - c * f
            data[2] = d

            data[4] = af + be * d
            data[5] = ae - bf * d
            data[6] = - b * c

            data[8] = bf - ae * d
            data[9] = be + af * d
            data[10] = a * c

        elif order == EulerOrder.YXZ:
            ce = c * e
            cf = c * f
            de = d * e
            df = d * f

            data[0] = ce + df * b
            data[1] = de * b - cf
            data[2] = a * d

            data[4] = a * f
            data[5] = a * e
            data[6] = - b

            data[8] = cf * b - de
            data[9] = df + ce * b
            data[10] = a * c

        elif order == EulerOrder.ZXY:
            ce = c * e
            cf = c * f
            de = d * e
            df = d * f

            data[0] = ce - df * b
            data[1] = - a * f
            data[2] = de + cf * b

            data[4] = cf + de * b
            data[5] = a * e
            data[6] = df - ce * b

            data[8] = - a * d
            data[9] = b
            data[10] = a * c

        elif order == EulerOrder.ZYX:
            ae = a * e
            af = a * f
            be = b * e
            bf = b * f

            data[0] = c * e
            data[1] = be * d - af
            data[2] = ae * d + bf

            data[4] = c * f
            data[5] = bf * d + ae
            data[6] = af * d - be

            data[8] = - d
            data[9] = b * c
            data[10] = a * c

        elif order == EulerOrder.YZX:
            ac = a * c
            ad = a * d
            bc = b * c
            bd = b * d

            data[0] = c * e
            data[1] = bd - ac * f
            data[2] = bc * f + ad

            data[4] = f
            data[5] = a * e
            data[6] = - b * e

            data[8] = - d * e
            data[9] = ad * f + bc
            data[10] = ac - bd * f

        elif order == EulerOrder.XZY:
            ac = a * c
            ad = a * d
            bc = b * c
            bd = b * d

            data[0] = c * e
            data[1] = - f
            data[2] = d * e

            data[4] = ac * f + bd
            data[5] = a * e
            data[6] = ad * f - bc

            data[8] = bc * f - ad
            data[9] = b * e
            data[10] = bd * f + ac

        # last column, unneeded, we initialized with zeroes
        # data[3] = 0
        # data[7] = 0
        # data[11] = 0

        # bottom row, unneeded, we initialized with zeroes
        # data[12] = 0
        # data[13] = 0
        # data[14] = 0
        data[15] = 1.0

        return cls(data).T

    @classmethod
    def from_quaternion(cls, quat: 'Quaternion') -> 'Matrix44':
        x = quat.x
        y = quat.y
        z = quat.z
        w = quat.w

        x2 = x + x
        y2 = y + y
        z2 = z + z
        xx = x * x2
        xy = x * y2
        xz = x * z2
        yy = y * y2
        yz = y * z2
        zz = z * z2
        wx = w * x2
        wy = w * y2
        wz = w * z2

        return cls(np.array([
            [1 - (yy + zz), xy - wz, xz + wy, 0.0],
            [xy + wz, 1 - (xx + zz), yz - wx, 0.0],
            [xz - wy, yz + wx, 1 - (xx + yy), 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ])).T

    @classmethod
    def from_axis_rotation(cls, axis: 'Vector3', theta: float) -> 'Matrix44':
        x, y, z = axis.normalized

        s = math.sin(theta)
        c = math.cos(theta)
        t = 1 - c

        return cls(np.array([
            [x * x * t + c, y * x * t + z * s, z * x * t - y * s, 0.0],
            [x * y * t - z * s, y * y * t + c, z * y * t + x * s, 0.0],
            [x * z * t + y * s, y * z * t - x * s, z * z * t + c, 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ]).T)

    @classmethod
    def from_x_rotation(cls, theta: float) -> 'Matrix44':
        cosT = math.cos(theta)
        sinT = math.sin(theta)
        return cls(np.array([
            [1.0, 0.0, 0.0, 0.0],
            [0.0, cosT, -sinT, 0.0],
            [0.0, sinT, cosT, 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ]).T)

    @classmethod
    def from_y_rotation(cls, theta: float) -> 'Matrix44':
        cosT = math.cos(theta)
        sinT = math.sin(theta)
        return cls(np.array([
            [cosT, 0.0, sinT, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [-sinT, 0.0, cosT, 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ]).T)

    @classmethod
    def from_z_rotation(cls, theta: float) -> 'Matrix44':
        cosT = math.cos(theta)
        sinT = math.sin(theta)
        return cls(np.array([
            [cosT, -sinT, 0.0, 0.0],
            [sinT, cosT, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ]).T)

    @classmethod
    def from_scale(cls, scale: 'Vector3') -> 'Matrix44':
        # return cls(np.diagflat([scale[0], scale[1], scale[2], 1.0]))
        return cls(np.array([
            [scale[0], 0.0, 0.0, 0.0],
            [0.0, scale[1], 0.0, 0.0],
            [0.0, 0.0, scale[2], 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ]))

    @classmethod
    def from_matrix33(cls, matrix: 'Matrix33') -> 'Matrix44':
        """
        Creates a Matrix44 from a Matrix33.
        """
        mat4 = np.identity(4)
        mat4[0:3, 0:3] = matrix
        return cls(mat4)

    @classmethod
    def perspective_projection(cls, fovy: float, aspect: float, near: float, far: float) -> 'Matrix44':
        """
        Creates a Matrix44 for use as a perspective projection matrix.

        .. seealso:: http://www.opengl.org/sdk/docs/man2/xhtml/gluPerspective.xml
        .. seealso:: http://www.geeks3d.com/20090729/howto-perspective-projection-matrix-in-opengl/

        :param float fovy: field of view in y direction in degrees
        :param float aspect: aspect ratio of the view (width / height)
        :param float near: distance from the viewer to the near clipping plane (only positive)
        :param float far: distance from the viewer to the far clipping plane (only positive)
        :rtype: numpy.array
        :return: A projection matrix representing the specified perpective.
        """
        ymax = near * math.tan(fovy * np.pi / 360.0)
        xmax = ymax * aspect
        return cls.perspective_projection_bounds(-xmax, xmax, -ymax, ymax, near, far)

    @classmethod
    def perspective_projection_bounds(cls, left: float, right: float, top: float, bottom: float, near: float, far: float) -> 'Matrix44':
        """
        Creates a Matrix44 for use as a perspective projection matrix.

        Creates a perspective projection matrix using the specified near
        plane dimensions.

        :param float left: The left of the near plane relative to the plane's centre.
        :param float right: The right of the near plane relative to the plane's centre.
        :param float top: The top of the near plane relative to the plane's centre.
        :param float bottom: The bottom of the near plane relative to the plane's centre.
        :param float near: The distance of the near plane from the camera's origin.
            It is recommended that the near plane is set to 1.0 or above to avoid rendering issues
            at close range.
        :param float far: The distance of the far plane from the camera's origin.
        :rtype: numpy.array
        :return: A projection matrix representing the specified perspective.

        .. seealso:: http://www.gamedev.net/topic/264248-building-a-projection-matrix-without-api/
        .. seealso:: http://www.glprogramming.com/red/chapter03.html

        """

        """
        E 0 A 0
        0 F B 0
        0 0 C D
        0 0-1 0

        A = (right+left)/(right-left)
        B = (top+bottom)/(top-bottom)
        C = -(far+near)/(far-near)
        D = -2*far*near/(far-near)
        E = 2*near/(right-left)
        F = 2*near/(top-bottom)
        """

        a = (right + left) / (right - left)
        b = (top + bottom) / (top - bottom)
        c = -(far + near) / (far - near)
        d = -2. * far * near / (far - near)
        e = 2. * near / (right - left)
        f = 2. * near / (top - bottom)

        return cls(np.array((
            (e, 0., 0., 0.),
            (0., f, 0., 0.),
            (a, b, c, -1.),
            (0., 0., d, 0.),
        )))

    @classmethod
    def orthogonal_projection(cls, left: float, right: float, top: float, bottom: float, near: float, far: float) -> 'Matrix44':
        """
        Creates a Matrix44 for use as an orthogonal projection matrix.

        Creates an orthogonal projection matrix.

        :param float left: The left of the near plane relative to the plane's centr`e.
        :param float right: The right of the near plane relative to the plane's centre.
        :param float top: The top of the near plane relative to the plane's centre.
        :param float bottom: The bottom of the near plane relative to the plane's centre.
        :param float near: The distance of the near plane from the camera's origin.
            It is recommended that the near plane is set to 1.0 or above to avoid rendering issues
            at close range.
        :param float far: The distance of the far plane from the camera's origin.
        :rtype: numpy.array
        :return: A projection matrix representing the specified orthogonal perspective.

        .. seealso:: http://msdn.microsoft.com/en-us/library/dd373965(v=vs.85).aspx`
        """

        """
        A 0 0 Tx
        0 B 0 Ty
        0 0 C Tz
        0 0 0 1

        A = 2 / (right - left)
        B = 2 / (top - bottom)
        C = -2 / (far - near)

        Tx = (right + left) / (right - left)
        Ty = (top + bottom) / (top - bottom)
        Tz = (far + near) / (far - near)
        """

        rml = right - left
        tmb = top - bottom
        fmn = far - near

        a = 2. / rml
        b = 2. / tmb
        c = -2. / fmn
        tx = -(right + left) / rml
        ty = -(top + bottom) / tmb
        tz = -(far + near) / fmn

        return cls(np.array((
            (a, 0., 0., 0.),
            (0., b, 0., 0.),
            (0., 0., c, 0.),
            (tx, ty, tz, 1.),
        )))

    @classmethod
    def look_at(cls, eye: 'Vector3', target: 'Vector3', up: 'Vector3') -> 'Matrix44':
        """
        Creates a Matrix44 for use as a lookAt matrix.

        :param numpy.array eye: Position of the camera in world coordinates.
        :param numpy.array target: The position in world coordinates that the camera is looking at.
        :param numpy.array up: The up vector of the camera.
        :rtype: numpy.array
        :return: A look at matrix that can be used as a viewMatrix
        """
        forward = (target - eye).normalized
        side = forward.cross(up).normalized
        up = side.cross(forward)

        return cls(np.array((
            (side[0], up[0], -forward[0], 0.),
            (side[1], up[1], -forward[1], 0.),
            (side[2], up[2], -forward[2], 0.),
            (-np.dot(side, eye), -np.dot(up, eye), np.dot(forward, eye), 1.0)
        )))

    @classmethod
    def from_translation(cls, translation: 'Vector3') -> 'Matrix44':
        """
        Creates a Matrix44 from the specified translation.
        """
        mat = Matrix44.identity()
        mat[3, 0:3] = translation[:3]
        return cls(mat)

    def __new__(cls, value: MathType = None, **kwargs: Any) -> 'Matrix44':
        if value is not None:
            if not isinstance(value, np.ndarray):
                obj = np.array(value)
            else:
                obj = value

            # # Matrix44 (Most common, here for performance, otherwise we test all the others)
            # if obj.shape == (4, 4,) or isinstance(obj, Matrix44):
            #     pass
            #
            # # Matrix33
            # elif obj.shape == (3, 3,) or isinstance(obj, Matrix33):
            #     obj = Matrix44.from_matrix33(obj)
            #
            # # Quaternion
            # elif obj.shape == (4,) or isinstance(obj, Quaternion):
            #     obj = Matrix44.from_quaternion(obj)
            #
            # # Euler
            # elif obj.shape == (3,) or isinstance(obj, Euler):
            #     obj = Matrix44.from_euler(obj)
        else:
            obj = np.identity(4)
        return super().__new__(cls, obj)

    def __truediv__(self, other: Any) -> None:
        self._unsupported_type('true divide', other)

    def __floordiv__(self, other: Any) -> None:
        self._unsupported_type('floor divide', other)

    def __div__(self, other: Any) -> None:
        self._unsupported_type('divide', other)

    def __invert__(self) -> 'Matrix44':
        return self.inverse

    def __add__(self, other: 'Matrix44') -> 'Matrix44':
        return Matrix44(super().__add__(other))

    def __sub__(self, other: 'Matrix44') -> 'Matrix44':
        return Matrix44(super().__sub__(other))

    def __mul__(self, other: 'Matrix44') -> 'Matrix44':
        return Matrix44(np.dot(other, self))

    def __ne__(self, other: 'Matrix44') -> bool:
        return bool(np.any(super().__ne__(other)))

    def __eq__(self, other: 'Matrix44') -> bool:
        return bool(np.all(super().__eq__(other)))

    def add_matrix33(self, other: Union['Matrix33', np.ndarray, List[float], Tuple[float, ...]]) -> 'Matrix44':
        return Matrix44(super().__add__(Matrix44.from_matrix33(other)))

    def sub_matrix33(self, other: Union['Matrix33', np.ndarray, List[float], Tuple[float, ...]]) -> 'Matrix44':
        return Matrix44(super().__sub__(Matrix44.from_matrix33(other)))

    def mul_matrix33(self, other: 'Matrix33') -> 'Matrix44':
        mat4 = np.identity(4)
        mat4[0:3, 0:3] = other
        return Matrix44(np.dot(mat4, self))

    def mul_quaternion(self, other: 'Quaternion') -> 'Matrix44':
        return self * other.matrix44

    def mul_vector3(self, other: 'Vector3') -> 'Vector3':
        vec4 = np.array([other[0], other[1], other[2], 1.])
        vec4 = np.dot(vec4, self)
        if np.allclose(vec4[3], 0.):
            vec4[:] = [np.inf, np.inf, np.inf, np.inf]
        else:
            vec4 /= vec4[3]
        return Vector3(vec4[:3])

    def mul_vector4(self, other: 'Vector4') -> 'Vector4':
        return Vector4(np.dot(other, self))

    def add_number(self, other: Number) -> 'Matrix44':
        return Matrix44(super().__add__(other))

    def sub_number(self, other: Number) -> 'Matrix44':
        return Matrix44(super().__sub__(other))

    def mul_number(self, other: Number) -> 'Matrix44':
        return Matrix44(super().__mul__(other))

    def div_number(self, other: Number) -> 'Matrix44':
        return Matrix44(super().__truediv__(other))

    @property
    def matrix33(self) -> 'Matrix33':
        return Matrix33.from_matrix44(self)

    @property
    def matrix44(self) -> 'Matrix44':
        return self

    @property
    def quaternion(self) -> 'Quaternion':
        return Quaternion.from_matrix(self)

    @property
    def inverse(self) -> 'Matrix44':
        return Matrix44(np.linalg.inv(self))

    @property
    def translation(self) -> 'Vector3':
        return Vector3.from_matrix44_translation(self)

    @translation.setter
    def translation(self, value: 'Vector3') -> None:
        self[3, 0:3] = value[:3]

    @property
    def scale(self) -> 'Vector3':
        scale = np.linalg.norm(self[:3, :3], axis=1)
        if np.linalg.det(self) < 0:
            scale[0] *= -1
        return Vector3(scale)

    def decompose(self) -> Tuple['Vector3', 'Quaternion', 'Vector3']:
        """
        Decomposes an affine transformation matrix into its scale, rotation and
        translation components.

        :param numpy.array m: A matrix.
        :return: tuple (rotation, translation, scale)
            Vector3 translation
            Quaternion rotation
            Vector3 scale
        """
        # NOTE: Copy is needed otherwise the reference is read only
        m = np.asarray(self).copy()
        scale = np.linalg.norm(m[:3, :3], axis=1)
        if np.linalg.det(m) < 0:
            scale[0] *= -1
        return \
            Vector3(m[3, :3]), \
            Matrix33(m[:3, :3] * (1 / scale)[:, None]).quaternion, \
            Vector3(scale)

    ...


from satmath.matrix33 import Matrix33
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satmath.vector4 import Vector4
from satmath.euler import Euler
