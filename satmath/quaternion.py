import math
from numbers import Number
from typing import Any, List, Tuple, Union, Optional

import numpy as np  # type: ignore

from satmath.base import BaseMatrix, BaseObject, BaseRotation, EulerOrder, MathType, NpProxy
from satmath.common import cross44, normalize


class Quaternion(BaseObject, BaseRotation):

    _shape = (4,)

    x = NpProxy(0)
    y = NpProxy(1)
    z = NpProxy(2)
    w = NpProxy(3)
    xy = NpProxy([0, 1])
    xyz = NpProxy([0, 1, 2])
    xyzw = NpProxy([0, 1, 2, 3])
    xz = NpProxy([0, 2])
    xzw = NpProxy([0, 2, 3])
    xyw = NpProxy([0, 1, 3])
    xw = NpProxy([0, 3])

    @classmethod
    def identity(cls) -> 'Quaternion':
        return cls([0.0, 0.0, 0.0, 1.0])

    @classmethod
    def from_x_rotation(cls, theta: float) -> 'Quaternion':
        """
        Creates a new Quaternion with a rotation around the X-axis.
        """
        theta_over2 = theta * 0.5
        return cls(np.array([
            math.sin(theta_over2),
            0.0,
            0.0,
            math.cos(theta_over2)
        ]))

    @classmethod
    def from_y_rotation(cls, theta: float) -> 'Quaternion':
        """
        Creates a new Quaternion with a rotation around the Y-axis.
        """
        theta_over2 = theta * 0.5
        return cls(np.array([
            0.0,
            math.sin(theta_over2),
            0.0,
            math.cos(theta_over2)
        ]))

    @classmethod
    def from_z_rotation(cls, theta: float) -> 'Quaternion':
        """
        Creates a new Quaternion with a rotation around the Z-axis.
        """
        theta_over2 = theta * 0.5
        return cls(np.array([
            0.0,
            0.0,
            math.sin(theta_over2),
            math.cos(theta_over2)
        ]))

    @classmethod
    def from_axis_rotation(cls, axis: 'Vector3', theta: float) -> 'Quaternion':
        """
        Creates a new Quaternion with a rotation around the specified axis.
        """
        # Make sure the vector is normalized
        if not np.isclose(np.linalg.norm(axis), 1.):
            axis = axis.normalized
        theta_over2 = theta * 0.5
        sin_theta_over2 = math.sin(theta_over2)
        return cls(np.array([
            sin_theta_over2 * axis[0],
            sin_theta_over2 * axis[1],
            sin_theta_over2 * axis[2],
            math.cos(theta_over2)
        ]))

    @classmethod
    def from_matrix(cls, matrix: 'BaseMatrix') -> 'Quaternion':
        """
        Creates a Quaternion from the specified Matrix44 (Matrix33 or Matrix44).
        """

        # assumes the upper 3x3 of m is a pure rotation matrix(i.e, unscaled)
        # the matrix needs to be translated to be equivalent
        # to its corresponding quaternion. This is because of the way sat-math
        # handles vector - matrix multiplication (ei A*b = b.T * A.T)
        matrix = matrix.T
        m11 = matrix[0][0]
        m12 = matrix[0][1]
        m13 = matrix[0][2]
        m21 = matrix[1][0]
        m22 = matrix[1][1]
        m23 = matrix[1][2]
        m31 = matrix[2][0]
        m32 = matrix[2][1]
        m33 = matrix[2][2]

        trace = m11 + m22 + m33

        if trace > 0:
            s = 0.5 / math.sqrt(trace + 1.0)
            return cls(np.array([
                (m32 - m23) * s,
                (m13 - m31) * s,
                (m21 - m12) * s,
                0.25 / s
            ]))

        elif m11 > m22 and m11 > m33:
            s = 2.0 * math.sqrt(1.0 + m11 - m22 - m33)
            return cls(np.array([
                0.25 * s,
                (m12 + m21) / s,
                (m13 + m31) / s,
                (m32 - m23) / s
            ]))

        elif m22 > m33:
            s = 2.0 * math.sqrt(1.0 + m22 - m11 - m33)
            return cls(np.array([
                (m12 + m21) / s,
                0.25 * s,
                (m23 + m32) / s,
                (m13 - m31) / s
            ]))

        else:
            s = 2.0 * math.sqrt(1.0 + m33 - m11 - m22)
            return cls(np.array([
                (m13 + m31) / s,
                (m23 + m32) / s,
                0.25 * s,
                (m21 - m12) / s
            ]))

    @classmethod
    def from_euler(cls, euler: 'Euler', order: Optional[EulerOrder] = None) -> 'Quaternion':
        """
        Creates a Quaternion from the specified Euler angles.
        """

        order = order or euler.order

        c1 = math.cos(euler.x / 2.0)
        c2 = math.cos(euler.y / 2.0)
        c3 = math.cos(euler.z / 2.0)

        s1 = math.sin(euler.x / 2.0)
        s2 = math.sin(euler.y / 2.0)
        s3 = math.sin(euler.z / 2.0)

        if order == EulerOrder.YXZ:
            return cls(np.array([
                s1 * c2 * c3 + c1 * s2 * s3,
                c1 * s2 * c3 - s1 * c2 * s3,
                c1 * c2 * s3 - s1 * s2 * c3,
                c1 * c2 * c3 + s1 * s2 * s3
            ]))

        elif order == EulerOrder.ZXY:
            return cls(np.array([
                s1 * c2 * c3 - c1 * s2 * s3,
                c1 * s2 * c3 + s1 * c2 * s3,
                c1 * c2 * s3 + s1 * s2 * c3,
                c1 * c2 * c3 - s1 * s2 * s3
            ]))

        elif order == EulerOrder.ZYX:
            return cls(np.array([
                s1 * c2 * c3 - c1 * s2 * s3,
                c1 * s2 * c3 + s1 * c2 * s3,
                c1 * c2 * s3 - s1 * s2 * c3,
                c1 * c2 * c3 + s1 * s2 * s3
            ]))

        elif order == EulerOrder.YZX:
            return cls(np.array([
                s1 * c2 * c3 + c1 * s2 * s3,
                c1 * s2 * c3 + s1 * c2 * s3,
                c1 * c2 * s3 - s1 * s2 * c3,
                c1 * c2 * c3 - s1 * s2 * s3
            ]))

        elif order == EulerOrder.XZY:
            return cls(np.array([
                s1 * c2 * c3 - c1 * s2 * s3,
                c1 * s2 * c3 - s1 * c2 * s3,
                c1 * c2 * s3 + s1 * s2 * c3,
                c1 * c2 * c3 + s1 * s2 * s3
            ]))

        # elif order == EulerOrder.XYZ:
        else:
            return cls(np.array([
                s1 * c2 * c3 + c1 * s2 * s3,
                c1 * s2 * c3 - s1 * c2 * s3,
                c1 * c2 * s3 + s1 * s2 * c3,
                c1 * c2 * c3 - s1 * s2 * s3
            ]))

    @classmethod
    def from_vector_track_up(cls, direction: 'Vector3', track: str = 'Y', up: str = 'Z') -> 'Quaternion':
        """
        Return a quaternion rotation from the vector and the track and up axis

        :arg direction: Direction vector
        :type direction: Vector3
        :arg track: Track axis in ['X', 'Y', 'Z', '-X', '-Y', '-Z'].
        :type track: string
        :arg up: Up axis in ['X', 'Y', 'Z'].
        :type up: string
        :return: rotation from the vector and the track and up axis.
        :rtype: :class:`Quaternion
        """
        # This code is based on Blender's to_track_quat implementation

        eps = 1e-4

        track_axes = ['X', 'Y', 'Z', '-X', '-Y', '-Z']
        try:
            r_track = track_axes.index(track)
        except ValueError:
            raise ValueError('Only X, -X, Y, -Y, Z or -Z for track axis')

        up_axes = ['X', 'Y', 'Z']
        try:
            r_up = up_axes.index(up)
        except ValueError:
            raise ValueError('Only X, Y or Z for up axis')

        if r_track == r_up:
            raise ValueError("Can't have the same axis for track and up")

        length = direction.length

        # We do not dare to process vectors too short
        if length < eps: # yes, arbitrary value
            raise ValueError('Direction vector is to short')

        if r_track > 2:
            tvec = -direction
            r_track = r_track - 3
        else:
            tvec = direction

        if r_track == 0:
            nor = Vector3((0.0, -tvec[2], tvec[1]))
            if abs(tvec[1]) + abs(tvec[2]) < eps:
                nor[1] = 1.0
            cos_angle = tvec[0]
        elif r_track == 1:
            nor = Vector3((tvec[2], 0.0, -tvec[0]))
            if abs(tvec[0]) + abs(tvec[2]) < eps:
                nor[2] = 1.0
            cos_angle = tvec[1]
        else:
            nor = Vector3((-tvec[1], tvec[0], 0.0))
            if abs(tvec[0]) + abs(tvec[1]) < eps:
                nor[0] = 1.0
            cos_angle = tvec[2]

        cos_angle = cos_angle / length
        nor.normalize()

        if cos_angle <= -1.0:
            angle = math.pi
        elif cos_angle >= 1.0:
            angle = 0.0
        else:
            angle = math.acos(cos_angle)

        quat = cls()
        quat[3] = math.cos(angle * 0.5)
        sin_angle = math.sin(angle * 0.5)
        quat[0] = nor[0] * sin_angle
        quat[1] = nor[1] * sin_angle
        quat[2] = nor[2] * sin_angle

        rot_mat = Matrix33.from_quaternion(quat)
        fp = rot_mat[2]
        if r_track == 0:
            if r_up == 1:
                angle = 0.5 * math.atan2(fp[2], fp[1])
            else:
                angle = -0.5 * math.atan2(fp[1], fp[2])
        elif r_track == 1:
            if r_up == 0:
                angle = -0.5 * math.atan2(fp[2], fp[0])
            else:
                angle = 0.5 * math.atan2(fp[0], fp[2])
        else:
            if r_up == 0:
                angle = 0.5 * math.atan2(-fp[1], -fp[0])
            else:
                angle = -0.5 * math.atan2(-fp[0], -fp[1])

        cos_angle = math.cos(angle)
        sin_angle = math.sin(angle) / length

        quat2 = cls()
        quat2[3] = cos_angle
        quat2[0] = tvec[0] * sin_angle
        quat2[1] = tvec[1] * sin_angle
        quat2[2] = tvec[2] * sin_angle

        quat = quat2 * quat

        return quat

    def __new__(cls, value: MathType = None, x: float = 0.0, y: float = 0.0, z: float = 0.0, w: float = 1.0, **kwargs: Any) -> 'Quaternion':
        if value is not None:
            if not isinstance(value, np.ndarray):
                obj = np.array(value)
            else:
                obj = value

            # # Quaternion (Most common, here for performance, otherwise we test all the others)
            # if obj.shape == (4,) or isinstance(obj, Quaternion):
            #     pass
            #
            # # Matrix33, Matrix44
            # elif obj.shape == (4, 4) or obj.shape == (3, 3) or isinstance(obj, (Matrix33, Matrix44)):
            #     obj = Quaternion.from_matrix(obj)
            #
            # # Euler
            # elif obj.shape == (3,) or isinstance(obj, Euler):
            #     obj = Quaternion.from_euler(obj)
        else:
            obj = np.array([x, y, z, w])
        return super().__new__(cls, obj)

    def __floordiv__(self, other: Any) -> None:
        self._unsupported_type('floor divide', other)

    def __div__(self, other: Any) -> None:
        self._unsupported_type('divide', other)

    def __add__(self, other: Any) -> None:
        self._unsupported_type('add', other)

    def __sub__(self, other: 'Quaternion') -> 'Quaternion':
        return Quaternion(super().__sub__(other))

    def __mul__(self, other: 'Quaternion') -> 'Quaternion':
        return self.cross(other)

    def __truediv__(self, other: 'Quaternion') -> 'Quaternion':
        return self * ~other

    def __or__(self, other: 'Quaternion') -> float:
        return float(self.dot(other))

    def __ne__(self, other: 'Quaternion') -> bool:
        return bool(np.any(super().__ne__(other)))

    def __eq__(self, other: 'Quaternion') -> bool:
        return bool(np.all(super().__eq__(other)))

    def __invert__(self) -> 'Quaternion':
        return self.conjugate.normalized

    def mul_matrix(self, other: BaseMatrix) -> 'Quaternion':
        return self * Quaternion.from_matrix(other)

    def mul_vector3(self, other: 'Vector3') -> 'Vector3':
        """
        Rotates a vector by a quaternion.
        """

        x = other.x
        y = other.y
        z = other.z
        qx = self.x
        qy = self.y
        qz = self.z
        qw = self.w

        # calculate quat * vector
        ix = qw * x + qy * z - qz * y
        iy = qw * y + qz * x - qx * z
        iz = qw * z + qx * y - qy * x
        iw = - qx * x - qy * y - qz * z

        # calculate result * inverse quat
        return Vector3(np.array([
            ix * qw + iw * - qx + iy * - qz - iz * - qy,
            iy * qw + iw * - qy + iz * - qx - ix * - qz,
            iz * qw + iw * - qz + ix * - qy - iy * - qx
        ]))

    def mul_vector4(self, other: 'Vector4') -> 'Vector4':
        """
        Rotates a vector by a quaternion.
        """
        return Vector4(cross44(self, cross44(other, self.conjugate)))

    def mul_number(self, other: Number) -> 'Quaternion':
        return Quaternion(super().__mul__(other))

    def div_number(self, other: Number) -> 'Quaternion':
        return Quaternion(super().__truediv__(other))

    @property
    def matrix33(self) -> 'Matrix33':
        return Matrix33.from_quaternion(self)

    @property
    def matrix44(self) -> 'Matrix44':
        return Matrix44.from_quaternion(self)

    @property
    def inverse(self) -> 'Quaternion':
        """
        The inverse of a quaternion is defined as
        the conjugate of the quaternion divided
        by the magnitude of the original quaternion.
        """
        return self.conjugate.normalized

    @property
    def length(self) -> float:
        """
        Returns the length of this Quaternion.
        """
        return float(np.sqrt(np.sum(np.square(self))).item(0))

    def normalize(self) -> None:
        """
        normalizes this Quaternion in-place.
        """
        self[:] = normalize(self)

    @property
    def normalized(self) -> 'Quaternion':
        """
        Returns a normalized version of this Quaternion as a new Quaternion.
        """
        return Quaternion(normalize(self))

    @property
    def angle(self) -> float:
        """
        Returns the angle around the axis of rotation of this Quaternion as a float.
        """
        return float(np.arccos(self.w) * 2.0)

    @property
    def axis(self) -> 'Vector3':
        """
        Returns the axis of rotation of this Quaternion as a Vector3.
        """
        sin_theta_over2_sq = 1.0 - (self.w ** 2)

        # check for zero before we sqrt
        if sin_theta_over2_sq <= 0.0:
            # identity quaternion or numerical imprecision.
            # return a valid vector
            # we'll treat -Z as the default
            return Vector3([0.0, 0.0, -1.0])

        one_over_sin_theta_over2 = 1.0 / np.sqrt(sin_theta_over2_sq)

        return Vector3(np.array([
            self.x * one_over_sin_theta_over2,
            self.y * one_over_sin_theta_over2,
            self.z * one_over_sin_theta_over2
        ]))

    def cross(self, other: 'Quaternion') -> 'Quaternion':
        """
        Returns the cross of this Quaternion and another.
        This is the equivalent of combining Quaternion rotations (like Matrix44 multiplication).
        Not done using numpy because it doesn't support dimensions other than 2 or 3
        """
        return Quaternion(cross44(self, other))

    def lerp(self, other: 'Quaternion', t: float) -> 'Quaternion':
        """
        Interpolates between self and other by t.
        The parameter t is clamped to the range [0, 1]
        """
        t = max(min(t, 1.0), 0.0)
        return Quaternion((self.mul_number(1.0 - t) + other.mul_number(t)).normalized)

    def slerp(self, other: 'Quaternion', t: float) -> 'Quaternion':
        """
        Spherically interpolates between self and other by t.
        The parameter t is clamped to the range [0, 1]
        """
        t = max(min(t, 1.0), 0.0)
        dot = self.dot(other)

        if dot < 0.0:
            dot = -dot
            quat3 = -other

        else:
            quat3 = other

        if dot < 0.95:
            angle = np.arccos(dot)
            res = (self * math.sin(angle * (1.0 - t)) + quat3 * math.sin(angle * t)) / math.sin(angle)

        else:
            res = self.lerp(other, t)

        return res

    @property
    def conjugate(self) -> 'Quaternion':
        """
        Returns the conjugate of this Quaternion.

        This is a Quaternion with the opposite rotation.
        """
        return Quaternion(np.array([-self.x, -self.y, -self.z, self.w]))

    def power(self, exponent: float) -> 'Quaternion':
        """
        Returns a new Quaternion representing this Quaternion to the power of the exponent.
        """
        # check for identify quaternion
        assert np.fabs(self.w) < 0.9999

        alpha = np.arccos(self.w)
        new_alpha = alpha * exponent
        multi = math.sin(new_alpha) / math.sin(alpha)

        return Quaternion(np.array([
            self.x * multi,
            self.y * multi,
            self.z * multi,
            math.cos(new_alpha)
        ]))

    @property
    def negative(self) -> 'Quaternion':
        """
        Returns the negative of the Quaternion.
        This is essentially the quaternion * -1.0.
        """
        return self.mul_number(-1.0)

    @property
    def is_identity(self) -> bool:
        """
        Returns True if the Quaternion has no rotation (0.,0.,0.,1.).
        """
        return bool(np.allclose(self, [0., 0., 0., 1.]))

    @property
    def is_zero_length(self) -> bool:
        """
        Checks if a quaternion is zero length.

        :param numpy.array quat: The quaternion to check.
        :rtype: boolean.
        :return: True if the quaternion is zero length, otherwise False.
        """
        return bool(self[0] == self[1] == self[2] == self[3] == 0.0)

    @property
    def is_non_zero_length(self) -> bool:
        """
        Checks if a quaternion is not zero length.

        This is the opposite to 'is_zero_length'.
        This is provided for readability.

        :param numpy.array quat: The quaternion to check.
        :rtype: boolean
        :return: False if the quaternion is zero length, otherwise True.

        .. seealso:: is_zero_length
        """
        return not self.is_zero_length

    @property
    def squared_length(self) -> float:
        return float(np.sum(np.square(self)).item(0))

    ...


from satmath.vector3 import Vector3
from satmath.vector4 import Vector4
from satmath.matrix33 import Matrix33
from satmath.matrix44 import Matrix44
from satmath.euler import Euler
