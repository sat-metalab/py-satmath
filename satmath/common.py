import numpy as np  # type: ignore


def normalize(vec: np.array) -> np.array:
    # return np.divide(vec, np.sqrt(np.sum(np.square(vec))))
    return np.divide(vec, np.linalg.norm(vec))


def cross44(a: np.array, b: np.array) -> np.array:
    ax, ay, az, aw = a
    bx, by, bz, bw = b
    return np.array([
        ax * bw + ay * bz - az * by + aw * bx,
        -ax * bz + ay * bw + az * bx + aw * by,
        ax * by - ay * bx + az * bw + aw * bz,
        -ax * bx - ay * by - az * bz + aw * bw,
    ])
