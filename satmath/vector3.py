import math
from numbers import Number
from typing import Any, List, Tuple, Union

import numpy as np  # type: ignore

from satmath.base import BaseVector, MathType, NpProxy


class Vector3(BaseVector):

    # region Numpy

    _shape = (3,)

    x = NpProxy(0)
    y = NpProxy(1)
    z = NpProxy(2)
    xy = NpProxy([0, 1])
    xyz = NpProxy([0, 1, 2])
    xz = NpProxy([0, 2])

    # endregion

    # region Creation

    @classmethod
    def from_vector4(cls, vector: Union['Vector4', np.ndarray]) -> 'Vector3':
        """
        Create a Vector3 from a Vector4.
        """
        # return cls(np.array([vector[0], vector[1], vector[2]]))
        return cls(vector[:3])

    @classmethod
    def from_matrix44_translation(cls, matrix: 'Matrix44') -> 'Vector3':
        return cls(np.array(matrix[3, :3]))

    @classmethod
    def from_matrix44_scale(cls, matrix: 'Matrix44') -> 'Vector3':
        m = np.asarray(matrix)
        scale = np.linalg.norm(m[:3, :3], axis=1)
        if np.linalg.det(m) < 0:
            scale[0] *= -1
        return cls(scale)

    @classmethod
    def x_axis(cls) -> 'Vector3':
        return cls([1.0, 0.0, 0.0])

    @classmethod
    def y_axis(cls) -> 'Vector3':
        return cls([0.0, 1.0, 0.0])

    @classmethod
    def z_axis(cls) -> 'Vector3':
        return cls([0.0, 0.0, 1.0])

    def __new__(cls, value: MathType = None, x: float = 0.0, y: float = 0.0, z: float = 0.0, **kwargs: Any) -> 'Vector3':
        if value is not None:
            if not isinstance(value, np.ndarray):
                obj = np.array(value)
            else:
                obj = value

            # # Vector3 (Most common, here for performance, otherwise we test all the others)
            # if obj.shape == (3,) or isinstance(obj, Vector3):
            #     pass
            #
            # # Vector4
            # elif obj.shape == (4,) or isinstance(obj, Vector4):
            #     obj = Vector3.from_vector4(obj)
            #
            # # Matrix44
            # elif obj.shape == (4, 4) or isinstance(obj, Matrix44):
            #     obj = Vector3.from_matrix44_translation(obj)
        else:
            obj = np.array([x, y, z])
        return super().__new__(cls, obj)

    # endregion

    # region Operators

    def __floordiv__(self, other: Any) -> None:
        self._unsupported_type('floor divide', other)

    def __div__(self, other: Any) -> None:
        self._unsupported_type('divide', other)

    def __add__(self, other: Union['Vector3', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Vector3':
        return Vector3(super().__add__(other))

    def __sub__(self, other: Union['Vector3', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Vector3':
        return Vector3(super().__sub__(other))

    def __mul__(self, other: Union['Vector3', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Vector3':
        return Vector3(super().__mul__(other))

    def __truediv__(self, other: Union['Vector3', np.ndarray, List[float], Tuple[float, ...], Number]) -> 'Vector3':
        return Vector3(super().__truediv__(other))

    def __xor__(self, other: 'Vector3') -> 'Vector3':
        return self.cross(other)

    def __or__(self, other: 'Vector3') -> float:
        return float(self.dot(other))

    def __ne__(self, other: 'Vector3') -> bool:
        return bool(np.any(super().__ne__(other)))

    def __eq__(self, other: 'Vector3') -> bool:
        return bool(np.all(super().__eq__(other)))

    # endregion

    # region Conversions

    @property
    def vector3(self) -> 'Vector3':
        return Vector3(self)

    @property
    def vector4(self) -> 'Vector4':
        return Vector4.from_vector3(self)

    def quaternion(self, track: str = 'Y', up: str = 'Z') -> 'Quaternion':
        return Quaternion.from_vector_track_up(self, track, up)

    # endregion

    # region Methods and Properties

    def cross(self, other: 'Vector3') -> 'Vector3':
        return Vector3(np.cross(self, other))

    @property
    def inverse(self) -> 'Vector3':
        return Vector3(-self)

    def angle(self, other: 'Vector3') -> float:
        """
        Returns the angle between this vector and another one
        :param other:Vector3
        :return: float
        """
        return math.acos(self.normalized.dot(other.normalized))

    # endregion
    ...


from satmath.vector4 import Vector4
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
