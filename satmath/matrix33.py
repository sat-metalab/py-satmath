import math
from numbers import Number
from typing import Union, List, Tuple, Any, Optional

import numpy as np  # type: ignore

from satmath.base import BaseMatrix, EulerOrder, MathType, NpProxy


class Matrix33(BaseMatrix):

    _shape = (3, 3)

    # m<c> style access
    m1 = NpProxy(0)
    m2 = NpProxy(1)
    m3 = NpProxy(2)

    # m<r><c> access
    m11 = NpProxy((0, 0))
    m12 = NpProxy((0, 1))
    m13 = NpProxy((0, 2))
    m21 = NpProxy((1, 0))
    m22 = NpProxy((1, 1))
    m23 = NpProxy((1, 2))
    m31 = NpProxy((2, 0))
    m32 = NpProxy((2, 1))
    m33 = NpProxy((2, 2))

    # rows
    r1 = NpProxy(0)
    r2 = NpProxy(1)
    r3 = NpProxy(2)

    # columns
    c1 = NpProxy((slice(0, 3), 0))
    c2 = NpProxy((slice(0, 3), 1))
    c3 = NpProxy((slice(0, 3), 2))

    @classmethod
    def identity(cls) -> 'Matrix33':
        return cls(np.identity(3))

    @classmethod
    def from_euler(cls, euler: 'Euler', order: Optional[EulerOrder] = None) -> 'Matrix33':
        assert isinstance(euler, Euler)

        order = order or euler.order

        a = math.cos(euler.x)
        b = math.sin(euler.x)
        c = math.cos(euler.y)
        d = math.sin(euler.y)
        e = math.cos(euler.z)
        f = math.sin(euler.z)

        data = np.zeros(9)

        if order == EulerOrder.XYZ:
            ae = a * e
            af = a * f
            be = b * e
            bf = b * f

            data[0] = c * e
            data[1] = - c * f
            data[2] = d

            data[3] = af + be * d
            data[4] = ae - bf * d
            data[5] = - b * c

            data[6] = bf - ae * d
            data[7] = be + af * d
            data[8] = a * c

        elif order == EulerOrder.YXZ:
            ce = c * e
            cf = c * f
            de = d * e
            df = d * f

            data[0] = ce + df * b
            data[1] = de * b - cf
            data[2] = a * d

            data[3] = a * f
            data[4] = a * e
            data[5] = - b

            data[6] = cf * b - de
            data[7] = df + ce * b
            data[8] = a * c

        elif order == EulerOrder.ZXY:
            ce = c * e
            cf = c * f
            de = d * e
            df = d * f

            data[0] = ce - df * b
            data[1] = - a * f
            data[2] = de + cf * b

            data[3] = cf + de * b
            data[4] = a * e
            data[5] = df - ce * b

            data[6] = - a * d
            data[7] = b
            data[8] = a * c

        elif order == EulerOrder.ZYX:
            ae = a * e
            af = a * f
            be = b * e
            bf = b * f

            data[0] = c * e
            data[1] = be * d - af
            data[2] = ae * d + bf

            data[3] = c * f
            data[4] = bf * d + ae
            data[5] = af * d - be

            data[6] = - d
            data[7] = b * c
            data[8] = a * c

        elif order == EulerOrder.YZX:
            ac = a * c
            ad = a * d
            bc = b * c
            bd = b * d

            data[0] = c * e
            data[1] = bd - ac * f
            data[2] = bc * f + ad

            data[3] = f
            data[4] = a * e
            data[5] = - b * e

            data[6] = - d * e
            data[7] = ad * f + bc
            data[8] = ac - bd * f

        elif order == EulerOrder.XZY:
            ac = a * c
            ad = a * d
            bc = b * c
            bd = b * d

            data[0] = c * e
            data[1] = - f
            data[2] = d * e

            data[3] = ac * f + bd
            data[4] = a * e
            data[5] = ad * f - bc

            data[6] = bc * f - ad
            data[7] = b * e
            data[8] = bd * f + ac

        return cls(data).T

    @classmethod
    def from_quaternion(cls, quat: 'Quaternion') -> 'Matrix33':
        x = quat.x
        y = quat.y
        z = quat.z
        w = quat.w

        x2 = x + x
        y2 = y + y
        z2 = z + z
        xx = x * x2
        xy = x * y2
        xz = x * z2
        yy = y * y2
        yz = y * z2
        zz = z * z2
        wx = w * x2
        wy = w * y2
        wz = w * z2

        return cls(np.array([
            [1 - (yy + zz), xy - wz, xz + wy],
            [xy + wz, 1 - (xx + zz), yz - wx],
            [xz - wy, yz + wx, 1 - (xx + yy)]
        ]).T)

    @classmethod
    def from_axis_rotation(cls, axis: 'Vector3', theta: float) -> 'Matrix33':
        x, y, z = axis.normalized

        s = math.sin(theta)
        c = math.cos(theta)
        t = 1 - c

        return cls(np.array([
            [x * x * t + c, y * x * t + z * s, z * x * t - y * s],
            [x * y * t - z * s, y * y * t + c, z * y * t + x * s],
            [x * z * t + y * s, y * z * t - x * s, z * z * t + c]
        ]).T)

    @classmethod
    def from_x_rotation(cls, theta: float) -> 'Matrix33':
        cosT = math.cos(theta)
        sinT = math.sin(theta)
        return cls(np.array([
            [1.0, 0.0, 0.0],
            [0.0, cosT, -sinT],
            [0.0, sinT, cosT]
        ]).T)

    @classmethod
    def from_y_rotation(cls, theta: float) -> 'Matrix33':
        cosT = math.cos(theta)
        sinT = math.sin(theta)
        return cls(np.array([
            [cosT, 0.0, sinT],
            [0.0, 1.0, 0.0],
            [-sinT, 0.0, cosT]
        ]).T)

    @classmethod
    def from_z_rotation(cls, theta: float) -> 'Matrix33':
        cosT = math.cos(theta)
        sinT = math.sin(theta)
        return cls(np.array([
            [cosT, -sinT, 0.0],
            [sinT, cosT, 0.0],
            [0.0, 0.0, 1.0]
        ]).T)

    @classmethod
    def from_scale(cls, scale: 'Vector3') -> 'Matrix33':
        return cls(np.array([
            [scale[0], 0.0, 0.0],
            [0.0, scale[1], 0.0],
            [0.0, 0.0, scale[2]]
        ]))

    @classmethod
    def from_matrix44(cls, matrix: 'Matrix44') -> 'Matrix33':
        """
        Creates a Matrix33 from a Matrix44.
        The Matrix44 translation will be lost.
        """
        return cls(np.asarray(matrix)[0:3, 0:3])

    def __new__(cls, value: MathType = None, **kwargs: Any) -> 'Matrix33':
        if value is not None:
            if not isinstance(value, np.ndarray):
                obj = np.array(value)
            else:
                obj = value

            # # Matrix33 (Most common, here for performance, otherwise we test all the others)
            # if obj.shape == (3, 3,) or isinstance(obj, Matrix33):
            #     pass
            #
            # # Matrix44
            # elif obj.shape == (4, 4,) or isinstance(obj, Matrix44):
            #     obj = Matrix33.from_matrix44(obj)
            #
            # # Quaternion
            # elif obj.shape == (4,) or isinstance(obj, Quaternion):
            #     obj = Matrix33.from_quaternion(obj)
        else:
            obj = np.identity(3)
        return super().__new__(cls, obj)

    def __truediv__(self, other: Any) -> None:
        self._unsupported_type('true divide', other)

    def __floordiv__(self, other: Any) -> None:
        self._unsupported_type('floor divide', other)

    def __div__(self, other: Any) -> None:
        self._unsupported_type('divide', other)

    def __invert__(self) -> 'Matrix33':
        return self.inverse

    def __add__(self, other: 'Matrix33') -> 'Matrix33':
        return Matrix33(super().__add__(other))

    def __sub__(self, other: 'Matrix33') -> 'Matrix33':
        return Matrix33(super().__sub__(other))

    def __mul__(self, other: 'Matrix33') -> 'Matrix33':
        return Matrix33(np.dot(other, self))

    def __ne__(self, other: Union['Matrix33', 'Matrix44']) -> bool:
        return bool(np.any(super().__ne__(other)))

    def __eq__(self, other: Union['Matrix33', 'Matrix44']) -> bool:
        return bool(np.all(super().__eq__(other)))

    def add_matrix44(self, other: Union['Matrix44', np.ndarray, List[float], Tuple[float, ...]]) -> 'Matrix33':
        return Matrix33(super().__add__(Matrix33.from_matrix44(other)))

    def sub_matrix44(self, other: Union['Matrix44', np.ndarray, List[float], Tuple[float, ...]]) -> 'Matrix33':
        return Matrix33(super().__sub__(Matrix33.from_matrix44(other)))

    def mul_matrix44(self, other: 'Matrix44') -> 'Matrix33':
        return Matrix33(np.dot(np.asarray(other)[0:3, 0:3], self))

    def mul_quaternion(self, other: 'Quaternion') -> 'Matrix33':
        return self * other.matrix33

    def mul_vector3(self, other: 'Vector3') -> 'Vector3':
        return Vector3(np.dot(other, self))

    def add_number(self, other: Number) -> 'Matrix33':
        return Matrix33(super().__add__(other))

    def sub_number(self, other: Number) -> 'Matrix33':
        return Matrix33(super().__sub__(other))

    def mul_number(self, other: Number) -> 'Matrix33':
        return Matrix33(super().__mul__(other))

    def div_number(self, other: Number) -> 'Matrix33':
        return Matrix33(super().__truediv__(other))

    @property
    def matrix33(self) -> 'Matrix33':
        return self

    @property
    def matrix44(self) -> 'Matrix44':
        return Matrix44.from_matrix33(self)

    @property
    def quaternion(self) -> 'Quaternion':
        return Quaternion.from_matrix(self)

    @property
    def inverse(self) -> 'Matrix33':
        return Matrix33(np.linalg.inv(self))

    ...


from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satmath.euler import Euler
