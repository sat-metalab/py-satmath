from abc import ABCMeta, abstractmethod
from enum import Enum
from typing import Union, List, Tuple, Type, TypeVar, Any, TYPE_CHECKING

import numpy as np  # type: ignore

from .common import normalize

if TYPE_CHECKING:
    from .euler import Euler
    from .vector3 import Vector3
    from .vector4 import Vector4
    from .quaternion import Quaternion
    from .matrix33 import Matrix33
    from .matrix44 import Matrix44

MathType = Union[List[float], Tuple[float, ...], np.ndarray]


class EulerOrder(Enum):
    XYZ = 0.0
    YZX = 1
    ZXY = 2
    XZY = 3
    YXZ = 4
    ZYX = 5


class NpProxy(object):
    """
    Numpy proxy, used to create various getters/views inside the numpy array on the classes
    """

    def __init__(self, index: Union[int, List[int], Tuple[int, ...], Tuple[slice, int], slice]) -> None:
        self._index = index

    def __get__(self, obj: np.ndarray, cls: Type[np.ndarray]) -> Union[np.ndarray, np.number]:
        return obj[self._index]

    def __set__(self, obj: np.ndarray, value: Union[np.ndarray, np.number]) -> None:
        obj[self._index] = value


BO = TypeVar('BO', bound='BaseObject')


class BaseObject(np.ndarray, metaclass=ABCMeta):
    """
    Base of all the objects in the library.
    Sets up the basics for using an `np.ndarray` as a base class
    """
    _shape = ()  # type: Tuple[int, ...]

    def __new__(cls: Type[BO], obj: np.ndarray) -> BO:
        view = obj.view(cls)  # type: BO
        # Ensure the object matches the required shape
        view.shape = cls._shape
        return view

    def _unsupported_type(self, method: str, other: Any) -> None:
        raise ValueError('Cannot {} a {} to/with a {}'.format(method, type(other).__name__, type(self).__name__))

    # Redirect assignment operators
    # NOTE: Numpy already defines those but we want to go through our own conversions

    def __iadd__(self: BO, other: MathType) -> BO:
        self[:] = self.__add__(other)
        return self

    def __isub__(self: BO, other: MathType) -> BO:
        self[:] = self.__sub__(other)
        return self

    def __imul__(self: BO, other: MathType) -> BO:
        self[:] = self.__mul__(other)
        return self

    def __itruediv__(self, other: MathType) -> BO:
        self[:] = self.__truediv__(other)
        return self

    # region Manipulations

    @property
    @abstractmethod
    def inverse(self: BO) -> BO:
        """
        Returns the inverse of this object.
        :return: BaseObject
        """
        raise NotImplementedError

    # endregion
    ...


BR = TypeVar('BR', bound='BaseRotation')


class BaseRotation:
    """
    Base interface for types that support rotation
    """

    # region Constructors

    @classmethod
    @abstractmethod
    def identity(cls: Type[BR]) -> BR:
        """
        Creates an identity rotation.
        :return: BaseRotation
        """
        raise NotImplementedError

    @classmethod
    def from_axis_rotation(cls: Type[BR], axis: 'Vector3', theta: float) -> BR:
        """
        :param axis: Vector3
        :param theta: float
        :return: BaseRotation
        """

    @classmethod
    @abstractmethod
    def from_x_rotation(cls: Type[BR], theta: float) -> BR:
        """
        Creates a rotation around the X-axis.
        :param theta: float
        :return: BaseRotation
        """
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def from_y_rotation(cls: Type[BR], theta: float) -> BR:
        """
        Creates a rotation around the Y-axis.
        :param theta: float
        :return: BaseRotation
        """
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def from_z_rotation(cls: Type[BR], theta: float) -> BR:
        """
        Creates a rotation around the Z-axis.
        :param theta: float
        :return: BaseRotation
        """
        raise NotImplementedError

    # endregion

    # region Conversions

    @property
    @abstractmethod
    def matrix33(self) -> 'Matrix33':
        """
        Returns the Matrix33.
        This can be handy if you're not sure what type of Matrix44 class you have but require a Matrix33.
        :return: Matrix33
        """
        raise NotImplementedError

    @property
    @abstractmethod
    def matrix44(self) -> 'Matrix44':
        """
        Returns a Matrix44 representing this matrix.
        This can be handy if you're not sure what type of Matrix44 class you have but require a Matrix44.
        :return: Matrix44
        """
        raise NotImplementedError

    # endregion

    # region Manipulations

    @property
    @abstractmethod
    def inverse(self: BR) -> BR:
        """
        Returns the inverse of this rotation.
        :return: BaseMatrix
        """
        raise NotImplementedError

    # endregion
    ...


BM = TypeVar('BM', bound='BaseMatrix')


class BaseMatrix(BaseObject, BaseRotation):
    """
    Base interface for all matrices
    """

    # region Constructors

    @classmethod
    @abstractmethod
    def from_euler(cls: Type[BM], euler: 'Euler') -> BM:
        """
        Creates a Matrix44 from the specified Euler angles.
        :param euler: Euler
        :return: BaseMatrix
        """
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def from_quaternion(cls: Type[BM], quat: 'Quaternion') -> BM:
        """
        Creates a Matrix44 from a Quaternion.
        :param quat: Quaternion
        :return: BaseMatrix
        """
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def from_scale(cls: Type[BM], scale: 'Vector3') -> BM:
        """
        Creates a Matrix44 from a Scale.
        :param scale: Vector3
        :return: BaseMatrix
        """
        raise NotImplementedError

    # endregion

    # region Conversions

    @property
    @abstractmethod
    def quaternion(self) -> 'Quaternion':
        """
        Returns a Quaternion representing this matrix.
        :return: Quaternion
        """
        raise NotImplementedError

    # endregion
    ...


BV = TypeVar('BV', bound='BaseVector')


class BaseVector(BaseObject):

    # region Constructors

    @classmethod
    @abstractmethod
    def from_matrix44_translation(cls: Type[BV], matrix: 'Matrix44') -> BV:
        pass

    @classmethod
    @abstractmethod
    def x_axis(cls: Type[BV]) -> BV:
        pass

    @classmethod
    @abstractmethod
    def y_axis(cls: Type[BV]) -> BV:
        pass

    @classmethod
    @abstractmethod
    def z_axis(cls: Type[BV]) -> BV:
        pass

    # endregion

    # region Properties

    @property
    def squared_length(self) -> float:
        return np.sum(np.square(self)).item(0)

    @property
    def length(self) -> float:
        return np.sqrt(np.sum(np.square(self))).item(0)

    @length.setter
    def length(self, length: float) -> None:
        self[:] = np.multiply(normalize(self), length)

    # endregion

    # region Conversions

    @property
    @abstractmethod
    def vector3(self) -> 'Vector3':
        """
        Returns the Vector3.
        This can be handy if you're not sure what type of Vector class you have but require a Vector3.
        :return: Vector3
        """
        raise NotImplementedError

    @property
    @abstractmethod
    def vector4(self) -> 'Vector4':
        """
        Returns the Vector4.
        This can be handy if you're not sure what type of Vector class you have but require a Vector4.
        :return: Vector4
        """
        raise NotImplementedError

    # endregion

    # region Manipulations

    def normalize(self) -> None:
        """
        Normalize the vector in place
        :return: None
        """
        self[:] = normalize(self)

    @property
    def normalized(self: BV) -> BV:
        """
        Return a normalized copy of this vector
        :return: BaseVector
        """
        return type(self)(normalize(self))

    @abstractmethod
    def cross(self: BV, other: BV) -> BV:
        pass

    def interpolate(self: BV, other: BV, delta: float) -> BV:
        return type(self)(self + ((other - self) * delta))

    def normal(self: BV, v2: BV, v3: BV, normalize_result: bool = True) -> BV:
        """
        Generates a normal vector for 3 vertices.

        The result is a normalized vector.

        It is assumed the ordering is counter-clockwise starting
        at v1, v2 then v3::

            v1      v3
              \    /
                v2

        The vertices are Nd arrays and may be 1d or Nd.
        As long as the final axis is of size 3.

        For 1d arrays::
            >>> v1 = numpy.array( [ 1.0, 0.0, 0.0 ] )
            >>> v2 = numpy.array( [ 0.0, 0.0, 0.0 ] )
            >>> v3 = numpy.array( [ 0.0, 1.0, 0.0 ] )
            >>> vector.generate_normals( v1, v2, v3 )
            array([ 0.,  0., -1.])

        For Nd arrays::
            >>> v1 = numpy.array( [ [ 1.0, 0.0, 0.0 ], [ 1.0, 0.0, 0.0 ] ] )
            >>> v2 = numpy.array( [ [ 0.0, 0.0, 0.0 ], [ 0.0, 0.0, 0.0 ] ] )
            >>> v3 = numpy.array( [ [ 0.0, 1.0, 0.0 ], [ 0.0, 1.0, 0.0 ] ] )
            >>> vector.generate_normals( v1, v2, v3 )
            array([[ 0.,  0., -1.],
                   [ 0.,  0., -1.]])

        :param numpy.array v1: an Nd array with the final dimension
            being size 3. (a vector)
        :param numpy.array v2: an Nd array with the final dimension
            being size 3. (a vector)
        :param numpy.array v3: an Nd array with the final dimension
            being size 3. (a vector)
        :param BVolean normalize_result: Specifies if the result should
            be normalized before being returned.
        """
        # make vectors relative to v2
        # we assume opengl counter-clockwise ordering
        a = self - v2
        b = v3 - v2
        n = b.cross(a)
        if normalize_result:
            n.normalize()
        return type(self)(n)

    # endregion
    ...
