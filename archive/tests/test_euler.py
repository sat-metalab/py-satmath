try:
    import unittest2 as unittest
except:
    import unittest
import numpy as np

from satmath.objects import Euler


class test_euler(unittest.TestCase):
    def test_import(self):
        import satmath
        satmath.euler

    def test_create(self):
        self.assertTrue(np.array_equal(Euler(), [0., 0., 0.]))
        e = Euler(roll=1., pitch=2., yaw=3.)
        self.assertEqual(1., e.roll)
        self.assertEqual(2., e.pitch)
        self.assertEqual(3., e.yaw)
        self.assertTrue(np.array_equal(e, [1., 2., 3.]))


if __name__ == '__main__':
    unittest.main()
