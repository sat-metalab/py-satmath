try:
    import unittest2 as unittest
except:
    import unittest
import numpy as np

from satmath.quaternion import Quaternion
from satmath.matrix44 import Matrix44
from satmath.matrix33 import Matrix33
from satmath.vector3 import Vector3
from satmath.vector4 import Vector4
from satmath.euler import Euler


class test_object_matrix33(unittest.TestCase):
    _shape = (3, 3)
    _size = np.multiply.reduce(_shape)

    def test_create(self):
        m = Matrix33()
        self.assertTrue(np.array_equal(m, np.identity(3)))
        self.assertEqual(m.shape, self._shape)

        m = Matrix33(np.arange(self._size))
        self.assertEqual(m.shape, self._shape)

        m = Matrix33([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
        self.assertEqual(m.shape, self._shape)

        m = Matrix33(Matrix33())
        self.assertTrue(np.array_equal(m, np.identity(3)))
        self.assertEqual(m.shape, self._shape)

    def test_identity(self):
        m = Matrix33.identity()
        self.assertTrue(np.array_equal(m, np.eye(3)))

    @unittest.skip('Not implemented')
    def test_perspective_projection(self):
        pass

    @unittest.skip('Not implemented')
    def test_perspective_projection_bounds(self):
        pass

    @unittest.skip('Not implemented')
    def test_orthogonal_projection(self):
        pass

    @unittest.skip('Not implemented')
    def test_from_translation(self):
        pass

    def test_create_from_matrix44(self):
        m1 = Matrix44.identity()
        m = Matrix33.from_matrix44(m1)
        self.assertTrue(np.array_equal(m, np.eye(3)))

        m = Matrix33.from_matrix44(m1)
        self.assertTrue(np.array_equal(m, np.eye(3)))

    def test_create_from_scale(self):
        v = Vector3([1, 2, 3])
        m = Matrix33.from_scale(v)
        self.assertTrue(np.array_equal(m, np.diag([1, 2, 3])))

    def test_create_from_euler(self):
        e = Euler([1, 2, 3])
        m = Matrix33.from_euler(e)
        self.assertTrue(np.allclose(m, np.array([
            [0.4119822, 0.0587266, 0.9092974],
            [-0.6812427, -0.6428728, 0.3501755],
            [0.6051273, -0.7637183, -0.2248451]
        ]).T))

    def test_create_from_quaternion(self):
        q = Quaternion()
        m = Matrix33.from_quaternion(q)
        self.assertTrue(np.array_equal(m, np.eye(3)))
        self.assertTrue(np.array_equal(m.quaternion, q))

        m = Matrix33.from_quaternion(q)
        self.assertTrue(np.array_equal(m, np.eye(3)))

    def test_create_from_inverse_quaternion(self):
        q = Quaternion.from_x_rotation(0.5)
        m = Matrix33.from_quaternion(q.inverse)
        self.assertTrue(np.allclose(m, np.array([
            [1, 0, 0],
            [0, 0.8775825, 0.4794256],
            [0, -0.4794256, 0.8775825]
        ]).T))

    def test_multiply(self):
        m1 = Matrix33(np.arange(self._size))
        m2 = Matrix33(np.arange(self._size)[::-1])
        m = m1 * m2
        self.assertTrue(np.array_equal(m, np.dot(m2, m1)))

        m1 = Matrix33(np.arange(self._size))
        m2 = Matrix44(np.arange(16))
        m = m1.mul_matrix44(m2)
        self.assertTrue(np.array_equal(m, np.dot(Matrix33.from_matrix44(m2), m1)))

    def test_inverse(self):
        m1 = Matrix33.identity() * Matrix33.from_x_rotation(0.5)
        m = m1.inverse
        self.assertTrue(np.array_equal(m, m1.inverse))

    def test_matrix33(self):
        m1 = Matrix33.identity() * Matrix33.from_x_rotation(0.5)
        m = m1.matrix33
        self.assertTrue(m1 is m)

    def test_matrix44(self):
        m1 = Matrix33.identity() * Matrix33.from_x_rotation(0.5)
        m = m1.matrix44
        self.assertTrue(np.array_equal(m, Matrix44.from_matrix33(m1)))

    def test_operators_matrix33(self):
        m1 = Matrix33.identity()
        m2 = Matrix33.from_x_rotation(0.5)

        # add
        self.assertTrue(np.array_equal(m1 + m2, Matrix33.identity() + Matrix33.from_x_rotation(0.5)))

        # subtract
        self.assertTrue(np.array_equal(m1 - m2, Matrix33.identity() - Matrix33.from_x_rotation(0.5)))

        # multiply
        self.assertTrue(np.array_equal(m1 * m2, np.dot(Matrix33.from_x_rotation(0.5), Matrix33.identity())))

        # divide
        self.assertRaises(ValueError, lambda: m1 / m2)

        # inverse
        self.assertTrue(np.array_equal(~m2, Matrix33.from_x_rotation(0.5).inverse))

        # ==
        self.assertTrue(Matrix33() == Matrix33())
        self.assertFalse(Matrix33() == Matrix33([1. for n in range(9)]))

        # !=
        self.assertTrue(Matrix33() != Matrix33([1. for n in range(9)]))
        self.assertFalse(Matrix33() != Matrix33())

    def test_operators_matrix44(self):
        m1 = Matrix33.identity()
        m2 = Matrix44.from_x_rotation(0.5)

        # add
        self.assertTrue(np.array_equal(m1.add_matrix44(m2), Matrix33.identity() + Matrix33.from_x_rotation(0.5)))

        # subtract
        self.assertTrue(np.array_equal(m1.sub_matrix44(m2), Matrix33.identity() - Matrix33.from_x_rotation(0.5)))

        # multiply
        self.assertTrue(np.array_equal(m1.mul_matrix44(m2), np.dot(Matrix33.from_x_rotation(0.5), Matrix33.identity())))

        # divide
        self.assertRaises(ValueError, lambda: m1 / m2)

    def test_operators_quaternion(self):
        m = Matrix33.identity()
        q = Quaternion.from_x_rotation(0.7)

        # add
        self.assertRaises(ValueError, lambda: m + q)

        # subtract
        self.assertRaises(ValueError, lambda: m - q)

        # multiply
        self.assertTrue(np.array_equal(m.mul_quaternion(q), np.dot(Matrix33.from_quaternion(Quaternion.from_x_rotation(0.7)), Matrix33.identity())))

        # divide
        self.assertRaises(ValueError, lambda: m / q)

    def test_operators_vector3(self):
        m = Matrix33.identity()
        v = Vector3([1, 1, 1])

        # add, not implemented
        # self.assertRaises(ValueError, lambda: m + v)

        # subtract, not implemented
        # self.assertRaises(ValueError, lambda: m - v)

        # multiply
        self.assertTrue(np.array_equal(m.mul_vector3(v), np.dot([1, 1, 1], Matrix33.identity())))
        self.assertTrue(np.allclose(Matrix33.from_x_rotation(np.pi / 2.).mul_vector3([0., 1., 0.]), [0., 0., 1.]))

        # divide
        self.assertRaises(ValueError, lambda: m / v)

    def test_operators_vector4(self):
        m = Matrix33.identity()
        v = Vector4([1, 1, 1, 1])

        # add
        self.assertRaises(ValueError, lambda: m + v)

        # subtract
        self.assertRaises(ValueError, lambda: m - v)

        # multiply
        self.assertTrue(ValueError, lambda: m * v)

        # divide
        self.assertRaises(ValueError, lambda: m / v)

    def test_operators_number(self):
        m = Matrix33.identity()
        fv = np.empty((1,), dtype=[('i', np.int16, 1), ('f', np.float32, 1)])
        fv[0] = (2, 2.0)

        # add
        self.assertTrue(np.array_equal(m.add_number(1.0), Matrix33.identity()[:] + 1.0))
        self.assertTrue(np.array_equal(m.add_number(1), Matrix33.identity()[:] + 1.0))
        self.assertTrue(np.array_equal(m.add_number(np.float(1.)), Matrix33.identity()[:] + 1.0))
        self.assertTrue(np.array_equal(m.add_number(fv[0]['f']), Matrix33.identity()[:] + 2.0))
        self.assertTrue(np.array_equal(m.add_number(fv[0]['i']), Matrix33.identity()[:] + 2.0))

        # subtract
        self.assertTrue(np.array_equal(m.sub_number(1.0), Matrix33.identity()[:] - 1.0))
        self.assertTrue(np.array_equal(m.sub_number(1), Matrix33.identity()[:] - 1.0))
        self.assertTrue(np.array_equal(m.sub_number(np.float(1.)), Matrix33.identity()[:] - 1.0))
        self.assertTrue(np.array_equal(m.sub_number(fv[0]['f']), Matrix33.identity()[:] - 2.0))
        self.assertTrue(np.array_equal(m.sub_number(fv[0]['i']), Matrix33.identity()[:] - 2.0))

        # multiply
        self.assertTrue(np.array_equal(m.mul_number(2.0), Matrix33.identity()[:] * 2.0))
        self.assertTrue(np.array_equal(m.mul_number(2), Matrix33.identity()[:] * 2.0))
        self.assertTrue(np.array_equal(m.mul_number(np.float(2.)), Matrix33.identity()[:] * 2.0))
        self.assertTrue(np.array_equal(m.mul_number(fv[0]['f']), Matrix33.identity()[:] * 2.0))
        self.assertTrue(np.array_equal(m.mul_number(fv[0]['i']), Matrix33.identity()[:] * 2.0))

        # divide
        self.assertTrue(np.array_equal(m.div_number(2.0), np.array(Matrix33.identity()[:]) / 2.0))
        self.assertTrue(np.array_equal(m.div_number(2), np.array(Matrix33.identity()[:]) / 2.0))
        self.assertTrue(np.array_equal(m.div_number(np.float(2.)), np.array(Matrix33.identity()[:]) / 2.0))
        self.assertTrue(np.array_equal(m.div_number(fv[0]['f']), np.array(Matrix33.identity()[:]) / 2.0))
        self.assertTrue(np.array_equal(m.div_number(fv[0]['i']), np.array(Matrix33.identity()[:]) / 2.0))

    def test_accessors(self):
        m = Matrix33(np.arange(self._size))
        self.assertTrue(np.array_equal(m.m1, [0, 1, 2]))
        self.assertTrue(np.array_equal(m.m2, [3, 4, 5]))
        self.assertTrue(np.array_equal(m.m3, [6, 7, 8]))

        self.assertTrue(np.array_equal(m.r1, [0, 1, 2]))
        self.assertTrue(np.array_equal(m.r2, [3, 4, 5]))
        self.assertTrue(np.array_equal(m.r3, [6, 7, 8]))

        self.assertTrue(np.array_equal(m.c1, [0, 3, 6]))
        self.assertTrue(np.array_equal(m.c2, [1, 4, 7]))
        self.assertTrue(np.array_equal(m.c3, [2, 5, 8]))

        self.assertEqual(m.m11, 0)
        self.assertEqual(m.m12, 1)
        self.assertEqual(m.m13, 2)
        self.assertEqual(m.m21, 3)
        self.assertEqual(m.m22, 4)
        self.assertEqual(m.m23, 5)
        self.assertEqual(m.m31, 6)
        self.assertEqual(m.m32, 7)
        self.assertEqual(m.m33, 8)

        m.m11 = 1
        self.assertEqual(m.m11, 1)
        self.assertEqual(m[0, 0], 1)
        m.m11 += 1
        self.assertEqual(m.m11, 2)
        self.assertEqual(m[0, 0], 2)


if __name__ == '__main__':
    unittest.main()
