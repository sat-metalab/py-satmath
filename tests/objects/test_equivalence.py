try:
    import unittest2 as unittest
except:
    import unittest
import numpy as np

from satmath.euler import Euler
from satmath.matrix33 import Matrix33
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3


class test_matrix_quaternion(unittest.TestCase):
    def test_m44_q_equivalence(self):
        """
        Test for equivalence of matrix and quaternion rotations.

        Create a matrix and quaternion, rotate each by the same values
        then convert matrix<->quaternion and check the results are the same.
        """
        m = Matrix44.from_x_rotation(np.pi / 2.)
        mq = Quaternion.from_matrix(m)

        q = Quaternion.from_x_rotation(np.pi / 2.)
        qm = Matrix44.from_quaternion(q)

        self.assertTrue(np.allclose(m.mul_vector4([0., 1., 0., 1.]), [0., 0., 1., 1.]))
        self.assertTrue(np.allclose(qm.mul_vector4([0., 1., 0., 1.]), [0., 0., 1., 1.]))

        self.assertTrue(np.allclose(q.mul_vector3(Vector3([0., 1., 0.])), [0., 0., 1.]))
        self.assertTrue(np.allclose(mq.mul_vector3(Vector3([0., 1., 0.])), [0., 0., 1.]))

        self.assertTrue(np.allclose(q,mq))
        self.assertTrue(np.allclose(m, qm))

    def test_euler_equivalence(self):
        euler = Euler.from_x_rotation(np.pi / 2.)
        m = Matrix33.from_x_rotation(np.pi / 2.)
        q = Quaternion.from_x_rotation(np.pi / 2.)
        qm = Matrix33.from_quaternion(q)
        em = Matrix33.from_euler(euler)

        self.assertTrue(np.allclose(qm, m))
        self.assertTrue(np.allclose(qm, em))
        self.assertTrue(np.allclose(m, em))

    def test_quaternion_matrix_conversion(self):
        # https://au.mathworks.com/help/robotics/ref/quat2rotm.html?requestedDomain=www.mathworks.com
        q = Quaternion([0.7071068, 0., 0., 0.7071068])
        m33 = q.matrix33
        expected = np.array([
            [1., 0., 0.],
            [0., -0., -1.],
            [0., 1., -0.],
        ]).T

        self.assertTrue(np.allclose(m33, expected, atol=1.e-5))

        # issue #42
        q = Quaternion([0.80087974, 0.03166748, 0.59114721, -0.09018753])
        m33 = q.matrix33
        q2 = Quaternion.from_matrix(m33)
        self.assertTrue(np.allclose(q, q2))

        q3 = Quaternion.from_matrix(m33.T)
        self.assertFalse(np.allclose(q2, q3))


if __name__ == '__main__':
    unittest.main()
